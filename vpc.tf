resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "subnet1a" {
  vpc_id            = aws_vpc.main.id
  availability_zone = var.az[0]
  cidr_block        = "10.0.1.0/24"

  tags = {
    Name = "1a"
  }
}

resource "aws_subnet" "subnet2a" {
  vpc_id            = aws_vpc.main.id
  availability_zone = var.az[0]
  cidr_block        = "10.0.2.0/24"

  tags = {
    Name = "2a"
  }
}

resource "aws_subnet" "subnet1b" {
  vpc_id            = aws_vpc.main.id
  availability_zone = var.az[1]
  cidr_block        = "10.0.3.0/24"

  tags = {
    Name = "1b"
  }
}

resource "aws_subnet" "subnet2b" {
  vpc_id            = aws_vpc.main.id
  availability_zone = var.az[1]
  cidr_block        = "10.0.4.0/24"

  tags = {
    Name = "2b"
  }
}
