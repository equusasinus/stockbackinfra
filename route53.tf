data "aws_route53_zone" "selected" {
  name         = var.dns_zone
  private_zone = false
}

resource "aws_route53_record" "equus" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.back_record_name
  type    = "A"
  ttl     = "300"
  records = [aws_eip.eip.public_ip]
}
